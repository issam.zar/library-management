Sprin Boot Library management

1- database init:
firstly we need to prepare database user before running the application, so please follow the instructions bellow:
1 -Access command line in your pc and enter this command:

    mysql -u root -p
    note: if root user configure to not use password you don't need -p

2 - create new schema name it: 'library-management':
    create database `library-management`;

3 -execute this command to create user with name: 'library' and password 'maidcc' like this: 

    CREATE USER 'library'@'localhost' IDENTIFIED BY 'maidcc';

4 - after creating user 'library', run this command to give him privileges to schema 'library-management' :
    GRANT ALL PRIVILEGES ON `library-management`. *  TO 'library'@'localhost';

after running these commands successfully, you can run our application.

When apllication is running,admin user was  saved to database this admin user info is : 
username -> admin
email -> admin@admin.com
password -> AdminP@ssw0rd

You can use this data to login and get access token :
 
first step you need to authintecate yourself to the application and get access token by hit this auth endpoints:

POST -> localhost:5000/api/auth/login and provide json object with your initial:
{

"usernameOremail":"admin",
"password":"AdminP@ssw0rd"

}

When getting access bearer token, we need to provide each request with this token in the Authorization header of the request.

****
Sign up new user:
- to signup new user you hit this endpoint :
POST -> localhost:5000/api/auth/signup with json object:
{
"username":"username",
"email":"user@gmail.com",
"password":"password"

}
Note : when using this api, new user will be created with role user by default, if admin user wants to change user role he(the admin) can hit this api: 
PUT -> localhost:5000/api/auth/{user id} with user json data to edit:
{
 "username":"name",
"email":"userexample@gmail.com",
 "admin":true

} 

Application endpoints:

For resource Book you have:
1-GET -> localhost:5000/api/books: retrive all books.
2-GET -> localhost:5000/api/books/{book id} : retrive book by its id .
3- POST -> localhost:5000/api/books -> add new book, only admin users can hit this api, you must provide this json object :
{
    "title":"Book title",
    "authorName":"Author name",
    "publicationYear":"year",
    "isbn":"ISBN************"
}

4- PUT -> localhost:5000/api/books/{book id to update} -> update book, only admin users can hit this api, you must provide this json object :
{
    "title":"updated Book title",
    "authorName":"Updated Author name",
    "publicationYear":"updated year",
    "isbn":"Updated ISBN************"
}

5- DELETE -> localhost:5000/api/books/{book id to delete} -> delete existing book if exist, only admin users can hit this api.


For resource Patron you have:

1- GET -> localhost:5000/api/patrons : retrive all patrons.
2- GET -> localhost:5000/api/patrons/{patron id} : retrive patron by its id.
3- POST -> localhost:5000/api/patrons : create new patron, only admin users can hit this api, you need to provide this json object:

{
   "name":"name",
   "phoneNo":"phone",
    "mobileNo":"mobile",
    "address":"address"
}

4- PUT -> localhost:5000/api/patrons/{patron id to update} : update patron, only admin users can hit this api, you need to provide this json object:

{
   "name":"updated name",
   "phoneNo":"updated phone",
    "mobileNo":"updated mobile",
    "address":"updated address"
}

5- DELETE ->localhost:5000/api/patrons/{patron id to delete} : delete patron if exist, only admin users can hit this api.

For resource Borrowing you have:

1- POST -> localhost:5000/api/borrow/{book id}/patron/{patron id} : create new book borrowing, only admin users can hit this api, you don't need to provide any data in request body.

2- PUT ->  localhost:5000/api/return/{book id}/patron/{patron id} : update book borrowing by specifying returning book, only admin users can hit this api, you must provide this json object:
{
    "returnDate":"yyyy-MM-dd"
}






