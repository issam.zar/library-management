package com.maidcc.library.model.service.impl;

import com.maidcc.library.model.entity.Book;
import com.maidcc.library.model.repository.BookRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class BookServiceImplTest {

    @Mock
    private BookRepository bookRepository;

    @Test
    void findAll() {
        when(bookRepository.findAll()).thenReturn(createDummyList());

        BookServiceImpl bookService=new BookServiceImpl(bookRepository);
        List<Book> list = bookService.findAll();
        assertNotNull(list);
        assertTrue(!list.isEmpty());
    }

    @Test
    void getOneById() {
        when(bookRepository.findById(1L)).thenReturn(Optional.ofNullable(createDummyBook()));
        BookServiceImpl bookService=new BookServiceImpl(bookRepository);
        Book existBook = bookService.getOneById(1L);

        assertTrue(existBook != null);
        assertTrue(existBook.getId() == 1L);
        assertEquals("Test Book Title", existBook.getTitle());
    }

    @Test
    void addNewOne() {
        when(bookRepository.save(any())).thenReturn(createDummyBook());

        BookServiceImpl bookService=new BookServiceImpl(bookRepository);
        Book book =new Book();

        Book addedBook=bookService.addNewOne(book);
        assertTrue(addedBook != null);
        assertTrue(addedBook.getId() == 1L);
    }

    @Test
    void updateOne() {
        when(bookRepository.findById(1L)).thenReturn(Optional.ofNullable(createDummyBook()));

        BookServiceImpl bookService=new BookServiceImpl(bookRepository);
        Book toUpdateBook = createDummyBook();
        toUpdateBook.setTitle("UpdateBook Title");
        when(bookRepository.save(any())).thenReturn(toUpdateBook);
        Book updatedBook = bookService.updateOne(1L,toUpdateBook);

        assertTrue(updatedBook != null);
        assertTrue(updatedBook.getId() == 1L);
        assertEquals("UpdateBook Title", updatedBook.getTitle());
    }

    @Test
    void deleteOne() {
        when(bookRepository.findById(1L)).thenReturn(Optional.ofNullable(createDummyBook()));
        doNothing().when(bookRepository).deleteById(1L);
        BookServiceImpl bookService=new BookServiceImpl(bookRepository);
        bookService.deleteOne(1L);
        verify(bookRepository, times(1)).deleteById(1L);
        verifyNoMoreInteractions(bookRepository);

    }

    Book createDummyBook(){
        Book book=new Book();
        book.setId(1L);
        book.setTitle("Test Book Title");
        return book;
    }

    List<Book> createDummyList(){
        List<Book> books=new ArrayList<>();
        books.add(createDummyBook());
        return books;
    }
}