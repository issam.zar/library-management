package com.maidcc.library.model.service.impl;

import com.maidcc.library.model.entity.Book;
import com.maidcc.library.model.entity.Patron;
import com.maidcc.library.model.repository.PatronRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class PatronServiceImplTest {

    @Mock
    private PatronRepository patronRepository;

    @Test
    void findAll() {
        when(patronRepository.findAll()).thenReturn(createDummyList());

        PatronServiceImpl patronService=new PatronServiceImpl(patronRepository);
        List<Patron> list = patronService.findAll();
        assertNotNull(list);
        assertTrue(!list.isEmpty());
    }

    @Test
    void getOneById() {
        when(patronRepository.findById(1L)).thenReturn(Optional.ofNullable(createDummyPatron()));
        PatronServiceImpl patronService=new PatronServiceImpl(patronRepository);
        Patron existPatron = patronService.getOneById(1L);

        assertTrue(existPatron != null);
        assertTrue(existPatron.getId() == 1L);
        assertEquals("Test Patron Name", existPatron.getName());
    }

    @Test
    void addNewOne() {
        when(patronRepository.save(any())).thenReturn(createDummyPatron());

        PatronServiceImpl patronService=new PatronServiceImpl(patronRepository);

        Patron patron=new Patron();
        Patron addedPatron = patronService.addNewOne(patron);
        assertTrue(addedPatron != null);
        assertTrue(addedPatron.getId() == 1L);
    }

    @Test
    void updateOne() {
        when(patronRepository.findById(1L)).thenReturn(Optional.ofNullable(createDummyPatron()));

        PatronServiceImpl patronService=new PatronServiceImpl(patronRepository);

        Patron toUpdatePatron = createDummyPatron();
        toUpdatePatron.setName("Updated Patron Name");
        when(patronRepository.save(any())).thenReturn(toUpdatePatron);
        Patron updatedPatron = patronService.updateOne(1L,toUpdatePatron);

        assertTrue(updatedPatron != null);
        assertTrue(updatedPatron.getId() == 1L);
        assertEquals("Updated Patron Name", updatedPatron.getName());
    }

    @DisplayName("Test delete existing patron by id")
    @Test
    void deleteOne() {
        when(patronRepository.findById(1L)).thenReturn(Optional.ofNullable(createDummyPatron()));
        doNothing().when(patronRepository).deleteById(1L);
        PatronServiceImpl patronService=new PatronServiceImpl(patronRepository);
        patronService.deleteOne(1L);
        verify(patronRepository, times(1)).deleteById(1L);
        verifyNoMoreInteractions(patronRepository);
    }

    Patron createDummyPatron(){
        Patron patron=new Patron();
        patron.setId(1L);
        patron.setName("Test Patron Name");
        return patron;
    }

    List<Patron> createDummyList(){
        List<Patron> patrons=new ArrayList<>();
        patrons.add(createDummyPatron());
        return patrons;
    }
}