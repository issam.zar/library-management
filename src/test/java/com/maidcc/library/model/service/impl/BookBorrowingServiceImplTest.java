package com.maidcc.library.model.service.impl;

import com.maidcc.library.dto.ReturnBorrowindDto;
import com.maidcc.library.model.entity.Book;
import com.maidcc.library.model.entity.BookBorrowing;
import com.maidcc.library.model.entity.Patron;
import com.maidcc.library.model.repository.BookBorrowingRepository;
import com.maidcc.library.model.repository.BookRepository;
import com.maidcc.library.model.repository.PatronRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BookBorrowingServiceImplTest {

    @Mock
    private BookBorrowingRepository borrowingRepository;
    @Mock
    private BookRepository bookRepository;
    @Mock
    private PatronRepository patronRepository;




    @Test
    void addNewBorrowing() {
        when(bookRepository.findById(1L)).thenReturn(Optional.ofNullable(createDummyBook()));
        when(patronRepository.findById(1L)).thenReturn(Optional.ofNullable(createDummyPatron()));
        when(borrowingRepository.save(any())).thenReturn(createDummyBorrowing());
        BookBorrowingServiceImpl borrowingService=new BookBorrowingServiceImpl(borrowingRepository,bookRepository,patronRepository);

        BookBorrowing newBorrowing=new BookBorrowing();
        BookBorrowing addedBorrowing=borrowingService.addNewBorrowing(1L , 1L);
        assertTrue(addedBorrowing != null);
        assertTrue(addedBorrowing.getId() == 1L);
        assertTrue(addedBorrowing.getBook() != null);
        assertTrue(addedBorrowing.getBook().getId() == 1L);
        assertTrue(addedBorrowing.getPatron() != null);
        assertTrue(addedBorrowing.getPatron().getId() == 1L);

    }

    @Test
    void returnBorrowing() {
        Book book = createDummyBook();
        Patron patron = createDummyPatron();
        when(bookRepository.findById(1L)).thenReturn(Optional.ofNullable(book));
        when(patronRepository.findById(1L)).thenReturn(Optional.ofNullable(patron));
        when(borrowingRepository.findByBookAndPatronAndReturningDateIsNull(book,patron)).thenReturn(createDummyBorrowing());
        BookBorrowingServiceImpl borrowingService=new BookBorrowingServiceImpl(borrowingRepository,bookRepository,patronRepository);

        BookBorrowing toReturnBorrow = createDummyBorrowing();
        toReturnBorrow.setReturningDate(LocalDate.now());
        when(borrowingRepository.save(any())).thenReturn(toReturnBorrow);

        BookBorrowing returnedBorrow = borrowingService.returnBorrowing(1L , 1L , createDummyReturnBorrowDto());
        assertTrue(returnedBorrow != null);
        assertTrue(returnedBorrow.getId() == 1L);
        assertTrue(returnedBorrow.getReturningDate() != null);

    }

    private ReturnBorrowindDto createDummyReturnBorrowDto() {
        ReturnBorrowindDto returnBorrowindDto=new ReturnBorrowindDto();
        returnBorrowindDto.setReturnDate(LocalDate.now());
        return returnBorrowindDto;
    }

    Book createDummyBook(){
        Book book=new Book();
        book.setId(1L);
        book.setTitle("Test Book Title");
        return book;
    }

    Patron createDummyPatron(){
        Patron patron=new Patron();
        patron.setId(1L);
        patron.setName("Test Patron Name");
        return patron;
    }

    BookBorrowing createDummyBorrowing(){
        BookBorrowing bookBorrowing=new BookBorrowing();
        bookBorrowing.setId(1L);
        bookBorrowing.setBook(createDummyBook());
        bookBorrowing.setPatron(createDummyPatron());
        return bookBorrowing;
    }
}