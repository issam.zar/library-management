package com.maidcc.library.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JWTResponseDto {
 private String accessToken;
 private String tokenType="Bearer";

    public JWTResponseDto(String accessToken) {
        this.accessToken = accessToken;
    }
}
