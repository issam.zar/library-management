package com.maidcc.library.dto;


import lombok.Data;

@Data
public class UpdateUserDto {
    private String username;
    private  String email;
    private boolean admin;
}
