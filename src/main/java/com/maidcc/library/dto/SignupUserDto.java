package com.maidcc.library.dto;

import lombok.Data;

@Data
public class SignupUserDto {
    private String username;
    private  String email;
    private String password;
}
