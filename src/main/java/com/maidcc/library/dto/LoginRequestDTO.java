package com.maidcc.library.dto;

import lombok.Data;

@Data
public class LoginRequestDTO {
    private String usernameOremail;
    private String password;
}
