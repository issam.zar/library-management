package com.maidcc.library.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ReturnBorrowindDto {
    private LocalDate returnDate;
}
