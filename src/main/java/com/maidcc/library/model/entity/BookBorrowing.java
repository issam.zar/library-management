package com.maidcc.library.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Table(name = "tbl_book_borrowing")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookBorrowing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private LocalDate returningDate;
    @ManyToOne
    @JoinColumn(name = "tbl_book_id")
    private Book book;
    @ManyToOne
    @JoinColumn(name = "tbl_patron_id")
    private Patron patron;
}
