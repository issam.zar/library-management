package com.maidcc.library.model.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "tbl_user")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private  String email;
    private String password;
    private boolean admin;
    @Transient
    private List<String> roles;


    public List<String> getRoles(){
        roles=new ArrayList<>();
        this.roles.add("ROLE_USER");
        if (this.admin)
           this.roles.add("ROLE_ADMIN");
        return roles;
    }

}
