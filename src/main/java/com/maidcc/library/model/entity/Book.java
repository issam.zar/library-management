package com.maidcc.library.model.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import java.util.List;

@Entity
@Table(name = "tbl_book")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "Book title must be provided!")
    @NotEmpty(message = "Book title must not be empty!")
    private String title;
    @NotNull(message = "Book Author name must be provided!")
    @NotEmpty(message = "Book Author must not be empty!")
    private String authorName;
    @NotEmpty(message = "Book publication year must not be empty!")
    @Pattern(regexp = "^\\d{4}$",message = "Enter valid publication year!")
    private String publicationYear;
    @NotNull(message = "Book ISBN must be provided!")
    @NotEmpty(message = "Book ISBN must not be empty!")
    private String isbn;
    @OneToMany(mappedBy = "book")
    @JsonIgnore
    private List<BookBorrowing> borrowingList;
}
