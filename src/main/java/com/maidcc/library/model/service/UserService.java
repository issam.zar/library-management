package com.maidcc.library.model.service;

import com.maidcc.library.dto.SignupUserDto;
import com.maidcc.library.dto.UpdateUserDto;
import com.maidcc.library.model.entity.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.Optional;

public interface UserService extends UserDetailsService {

    User addOne(User user);
    Optional<User> findUserByUsername(String username);

    Optional<User> findUserByEmail(String email);
    Optional<User> findUserByUsernameOrEmail(String username,String email);

    UserDetails loadUserByUsername(String username);

    void signupUser(SignupUserDto signupUserDto) throws Exception;

    UpdateUserDto updateUser(long id,UpdateUserDto updateUserDto);
}
