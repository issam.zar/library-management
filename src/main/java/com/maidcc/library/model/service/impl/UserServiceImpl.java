package com.maidcc.library.model.service.impl;

import com.maidcc.library.dto.SignupUserDto;
import com.maidcc.library.dto.UpdateUserDto;
import com.maidcc.library.exception.ResourceNotFoundException;
import com.maidcc.library.model.entity.User;
import com.maidcc.library.model.repository.UserRepository;
import com.maidcc.library.model.service.UserService;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService  {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder=passwordEncoder;
    }



    @Transactional(readOnly = true)
    @Override
    public Optional<User> findUserByUsername(String username) {
        return this.userRepository.findUserByUsername(username);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<User> findUserByEmail(String email) {
        return this.userRepository.findUserByEmail(email);
    }

    @Transactional(readOnly = true)
    @Override
    public Optional<User> findUserByUsernameOrEmail(String username, String email) {
        return this.userRepository.findUserByUsernameOrEmail(username,email);
    }

    @Transactional(readOnly = true)
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user=userRepository.findUserByUsernameOrEmail(username,username)
                .orElseThrow(()->new UsernameNotFoundException("User is not found with username/email!"));
        org.springframework.security.core.userdetails.User userDetail=new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                user.getRoles().stream()
                        .map(role-> new SimpleGrantedAuthority(role))
                        .collect(Collectors.toList()));

        return userDetail;
    }

    @Transactional
    @Override
    public void signupUser(SignupUserDto signupUserDto) throws Exception {
        Optional<User> user = this.userRepository.findUserByUsernameOrEmail(signupUserDto.getUsername(), signupUserDto.getEmail());
        if (user.isPresent())
            throw new Exception("Username or Email exist!");
        User newUser = new User();
        newUser.setUsername(signupUserDto.getUsername());
        newUser.setEmail(signupUserDto.getEmail());
        newUser.setPassword(passwordEncoder.encode(signupUserDto.getPassword()));
        this.userRepository.save(newUser);
    }


    @Transactional
    @Override
    public UpdateUserDto updateUser(long id,UpdateUserDto updateUserDto) {
        User existingUser = this.userRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("User not found with Id: "+id));
        existingUser.setEmail(updateUserDto.getEmail());
        existingUser.setUsername(updateUserDto.getUsername());
        existingUser.setAdmin(updateUserDto.isAdmin());
        this.userRepository.save(existingUser);
        return updateUserDto;
    }

    @Transactional
    @Override
    public User addOne(User user) {
        return this.userRepository.save(user);
    }
}
