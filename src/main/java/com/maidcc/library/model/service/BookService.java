package com.maidcc.library.model.service;

import com.maidcc.library.model.entity.Book;

import java.util.List;

public interface BookService {

    List<Book> findAll();

    Book getOneById(long id);
    Book addNewOne(Book book);
    Book updateOne(long id,Book book);

    void deleteOne(long id);


}
