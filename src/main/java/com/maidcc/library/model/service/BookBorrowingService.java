package com.maidcc.library.model.service;

import com.maidcc.library.dto.ReturnBorrowindDto;
import com.maidcc.library.model.entity.BookBorrowing;

import java.util.List;

public interface BookBorrowingService {
    List<BookBorrowing> findAll();

    BookBorrowing getOneById(long id);
    BookBorrowing addNewOne(BookBorrowing bookBorrowing);
    BookBorrowing updateOne(long id,BookBorrowing bookBorrowing);

    void deleteOne(long id);

    BookBorrowing returnBorrowing(long bookId, long patronId, ReturnBorrowindDto returnBorrowindDto);

    BookBorrowing addNewBorrowing(long bookId, long patronId);
}
