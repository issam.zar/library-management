package com.maidcc.library.model.service.impl;

import com.maidcc.library.dto.ReturnBorrowindDto;
import com.maidcc.library.exception.ResourceNotFoundException;
import com.maidcc.library.model.entity.Book;
import com.maidcc.library.model.entity.BookBorrowing;
import com.maidcc.library.model.entity.Patron;
import com.maidcc.library.model.repository.BookBorrowingRepository;
import com.maidcc.library.model.repository.BookRepository;
import com.maidcc.library.model.repository.PatronRepository;
import com.maidcc.library.model.service.BookBorrowingService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookBorrowingServiceImpl implements BookBorrowingService {

    private BookBorrowingRepository borrowingRepository;
    private BookRepository bookRepository;
    private PatronRepository patronRepository;

    public BookBorrowingServiceImpl(BookBorrowingRepository borrowingRepository, BookRepository bookRepository, PatronRepository patronRepository) {
        this.borrowingRepository = borrowingRepository;
        this.bookRepository = bookRepository;
        this.patronRepository = patronRepository;
    }

    @Transactional
    @Override
    public List<BookBorrowing> findAll() {
        return this.borrowingRepository.findAll();
    }

    @Transactional
    @Override
    public BookBorrowing getOneById(long id) {
        BookBorrowing bookBorrowing = this.borrowingRepository.findById(id).get();
        return bookBorrowing;
    }

    @Transactional
    @Override
    public BookBorrowing addNewOne(BookBorrowing bookBorrowing) {
        BookBorrowing addedBookBorrowing = this.borrowingRepository.save(bookBorrowing);

        return addedBookBorrowing;
    }

    @Transactional
    @Override
    public BookBorrowing updateOne(long id, BookBorrowing bookBorrowing) {
        BookBorrowing existBorrowing = this.borrowingRepository.findById(id).get();
        existBorrowing.setReturningDate(bookBorrowing.getReturningDate());
        existBorrowing.setBook(bookBorrowing.getBook());
        existBorrowing.setPatron(bookBorrowing.getPatron());

        BookBorrowing updatedBorrowing = this.borrowingRepository.save(existBorrowing);
        return updatedBorrowing;
    }

    @Transactional
    @Override
    public void deleteOne(long id) {
        BookBorrowing existBorrowing = this.borrowingRepository.findById(id).get();
        this.borrowingRepository.deleteById(id);
    }

    @Transactional
    @Override
    public BookBorrowing returnBorrowing(long bookId, long patronId, ReturnBorrowindDto returnBorrowindDto) {
        Book book = this.bookRepository.findById(bookId).orElseThrow(() -> new ResourceNotFoundException("Book not exist with Id: " + bookId));
        Patron patron = this.patronRepository.findById(patronId).orElseThrow(() -> new ResourceNotFoundException("Patron not exist with Id: " + patronId));
        BookBorrowing existBorrowing = this.borrowingRepository.findByBookAndPatronAndReturningDateIsNull(book, patron);
        if (existBorrowing != null) {
            existBorrowing.setReturningDate(returnBorrowindDto.getReturnDate());
            BookBorrowing returnedBorrow=this.borrowingRepository.save(existBorrowing);
            return  returnedBorrow;
        } else
            throw new ResourceNotFoundException("There is no Borrowing exist for book title: " + book.getTitle() + " and Patron name: " + patron.getName());
    }

    @Transactional
    @Override
    public BookBorrowing addNewBorrowing(long bookId, long patronId) {
        Book book = this.bookRepository.findById(bookId).orElseThrow(() -> new ResourceNotFoundException("Book not exist with Id: " + bookId));
        Patron patron = this.patronRepository.findById(patronId).orElseThrow(() -> new ResourceNotFoundException("Patron not exist with Id: " + patronId));
        BookBorrowing borrowing = new BookBorrowing();
        borrowing.setBook(book);
        borrowing.setPatron(patron);
        BookBorrowing addedBorrowing = this.borrowingRepository.save(borrowing);
        return addedBorrowing;
    }
}
