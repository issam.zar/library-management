package com.maidcc.library.model.service.impl;

import com.maidcc.library.exception.ResourceNotFoundException;
import com.maidcc.library.model.entity.Book;
import com.maidcc.library.model.repository.BookRepository;
import com.maidcc.library.model.service.BookService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {


    private BookRepository bookRepository;


    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Book> findAll() {
        return this.bookRepository.findAll();
    }

    @Cacheable("books_cache")
    @Transactional(readOnly = true)
    @Override
    public Book getOneById(long id) {
        Book book=this.bookRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("Book not found with Id: "+id));
        return book;
    }

    @Transactional
    @Override
    public Book addNewOne(Book book) {
        Book addedBook = this.bookRepository.save(book);
        return addedBook;
    }

    @Transactional
    @Override
    public Book updateOne(long id,Book book) {
        Book existingBook = this.bookRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Book not found with Id: "+id));
        existingBook.setTitle(book.getTitle());
        existingBook.setAuthorName(book.getAuthorName());
        existingBook.setIsbn(book.getIsbn());
        existingBook.setPublicationYear(book.getPublicationYear());
        Book updatedBook = this.bookRepository.save(existingBook);

        return updatedBook;
    }

    @Transactional
    @Override
    public void deleteOne(long id) {
        Book existingBook = this.bookRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Book not found with Id: "+id));
        this.bookRepository.deleteById(id);
    }
}
