package com.maidcc.library.model.service;

import com.maidcc.library.model.entity.Patron;

import java.util.List;

public interface PatronService {

    List<Patron> findAll();

    Patron getOneById(long id);
    Patron addNewOne(Patron Patron);
    Patron updateOne(long id,Patron Patron);

    void deleteOne(long id);
}
