package com.maidcc.library.model.service.impl;


import com.maidcc.library.exception.ResourceNotFoundException;
import com.maidcc.library.model.entity.Patron;
import com.maidcc.library.model.repository.PatronRepository;
import com.maidcc.library.model.service.PatronService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class PatronServiceImpl implements PatronService {


    private PatronRepository patronRepository;

    public PatronServiceImpl(PatronRepository patronRepository) {
        this.patronRepository = patronRepository;
    }

    @Transactional(readOnly = true)
    @Override
    public List<Patron> findAll() {
        return this.patronRepository.findAll();
    }


    @Cacheable("patrons_cache")
    @Transactional(readOnly = true)
    @Override
    public Patron getOneById(long id) {
        return patronRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Patron not found with Id: "+id));
    }

    @Transactional
    @Override
    public Patron addNewOne(Patron patron) {
        Patron addedPatron = this.patronRepository.save(patron);
        return addedPatron;
    }

    @Transactional
    @Override
    public Patron updateOne(long id, Patron patron) {
        Patron existPatron=this.patronRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Patron not found with Id: "+id));
        existPatron.setName(patron.getName());
        existPatron.setPhoneNo(patron.getPhoneNo());
        existPatron.setMobileNo(patron.getMobileNo());
        existPatron.setAddress(patron.getAddress());

        Patron updatedPatron = this.patronRepository.save(existPatron);
        return updatedPatron;
    }

    @Transactional
    @Override
    public void deleteOne(long id) {
        Patron existPatron=this.patronRepository.findById(id).orElseThrow(()-> new ResourceNotFoundException("Patron not found with Id: "+id));

        this.patronRepository.deleteById(id);
    }
}
