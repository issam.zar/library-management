package com.maidcc.library.model.repository;

import com.maidcc.library.model.entity.Book;
import com.maidcc.library.model.entity.BookBorrowing;
import com.maidcc.library.model.entity.Patron;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookBorrowingRepository extends JpaRepository<BookBorrowing,Long> {
    BookBorrowing findByBookAndPatronAndReturningDateIsNull(Book book, Patron patron);
}
