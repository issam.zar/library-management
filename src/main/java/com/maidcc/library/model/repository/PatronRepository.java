package com.maidcc.library.model.repository;

import com.maidcc.library.model.entity.Patron;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PatronRepository extends JpaRepository<Patron,Long> {
}
