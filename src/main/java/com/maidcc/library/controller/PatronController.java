package com.maidcc.library.controller;


import com.maidcc.library.model.entity.Patron;
import com.maidcc.library.model.service.PatronService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/patrons")
public class PatronController {
    private PatronService patronService;

    public PatronController(PatronService patronService) {
        this.patronService = patronService;
    }

    @GetMapping()
    public List<Patron> retriveAllPatrons(){
        return this.patronService.findAll();
    }

    @GetMapping("/{id}")
    public Patron retrivePatronById(@PathVariable("id") long id){
        Patron patron = this.patronService.getOneById(id);
        return patron;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping()
    public Patron addNewPatron(@RequestBody Patron patron){
        Patron addedPatron = this.patronService.addNewOne(patron);
        return addedPatron;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public Patron updatePatron(@PathVariable("id") long id , @RequestBody Patron patron){
        Patron updatedPatron = this.patronService.updateOne(id,patron);
        return updatedPatron;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public String deletePatron(@PathVariable("id") long id){
        this.patronService.deleteOne(id);
        return "Patron deleted successfully";
    }
}
