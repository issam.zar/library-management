package com.maidcc.library.controller;

import com.maidcc.library.dto.JWTResponseDto;
import com.maidcc.library.dto.LoginRequestDTO;
import com.maidcc.library.dto.SignupUserDto;
import com.maidcc.library.dto.UpdateUserDto;
import com.maidcc.library.model.entity.User;
import com.maidcc.library.model.service.UserService;
import com.maidcc.library.security.JWTProvider;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("api/auth")
public class AuthController {

    private UserService userService;

    private AuthenticationManager authenticationManager;
    private JWTProvider jwtProvider;

    public AuthController(UserService userService, AuthenticationManager authenticationManager, JWTProvider jwtProvider) {
        this.userService = userService;

        this.authenticationManager = authenticationManager;
        this.jwtProvider = jwtProvider;
    }

    @PostMapping("/login")
    public ResponseEntity<JWTResponseDto> login(@RequestBody LoginRequestDTO loginRequestDTO) {

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequestDTO.getUsernameOremail(), loginRequestDTO.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = jwtProvider.generateToken(authentication);

        return ResponseEntity.ok(new JWTResponseDto(token));
    }

    @PostMapping("/signup")
    public String signup(@RequestBody SignupUserDto signupUserDto) throws Exception {
       userService.signupUser(signupUserDto);
        return "success";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public UpdateUserDto signup(@PathVariable long id, @RequestBody UpdateUserDto updateUserDto) throws Exception {
         return userService.updateUser(id,updateUserDto);
    }




}
