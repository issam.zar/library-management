package com.maidcc.library.controller;

import com.maidcc.library.model.entity.Book;
import com.maidcc.library.model.service.BookService;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/books")
public class BookController {

    private BookService bookService;

    public BookController(BookService bookService) {
        this.bookService = bookService;
    }

    @GetMapping()
    public List<Book> retriveAllBooks(){
        return this.bookService.findAll();
    }

    @GetMapping("/{id}")
    public Book retriveBookById(@PathVariable("id") long id){
        Book book = this.bookService.getOneById(id);
        return book;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping()
    public Book addNewBook(@RequestBody @Valid Book book){
        Book addedBook = this.bookService.addNewOne(book);
        return addedBook;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/{id}")
    public Book updateBook(@PathVariable("id") long id , @RequestBody Book book){
        Book updatedBook = this.bookService.updateOne(id,book);
        return updatedBook;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public String deleteBook(@PathVariable("id") long id){
        this.bookService.deleteOne(id);
        return "Book deleted successfully";
    }
}
