package com.maidcc.library.controller;

import com.maidcc.library.dto.ReturnBorrowindDto;
import com.maidcc.library.model.entity.BookBorrowing;
import com.maidcc.library.model.service.BookBorrowingService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api")
public class BookBorrowingController {

    private BookBorrowingService borrowingService;

    public BookBorrowingController(BookBorrowingService borrowingService) {
        this.borrowingService = borrowingService;
    }


    @GetMapping()
    public List<BookBorrowing> retriveAllBorrowings(){
        return this.borrowingService.findAll();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/borrow/{bookId}/patron/{patronId}")
    public BookBorrowing addNewBorrowing(@PathVariable("bookId") long bookId,
                                         @PathVariable("patronId") long patronId){
        return this.borrowingService.addNewBorrowing(bookId,patronId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/return/{bookId}/patron/{patronId}")
    public BookBorrowing returnBorrowing(@PathVariable("bookId") long bookId,
                                         @PathVariable("patronId") long patronId,
                                         @RequestBody ReturnBorrowindDto returnBorrowindDto){
        return this.borrowingService.returnBorrowing(bookId,patronId,returnBorrowindDto);
    }




}
