package com.maidcc.library.util;


import com.maidcc.library.model.entity.User;
import com.maidcc.library.model.service.UserService;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class InitiDataUtil implements ApplicationRunner {

    private final UserService userService;
    private final PasswordEncoder passwordEncoder;

    public InitiDataUtil(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public void run(ApplicationArguments args) throws Exception {
        Optional<User> admin = userService.findUserByUsername("admin");
        if(!admin.isPresent()){
            User adminUser=new User();
            adminUser.setUsername("admin");
            adminUser.setEmail("admin@admin.com");
            adminUser.setPassword(passwordEncoder.encode("AdminP@ssw0rd"));
            adminUser.setAdmin(true);
            userService.addOne(adminUser);
        }

    }
}
