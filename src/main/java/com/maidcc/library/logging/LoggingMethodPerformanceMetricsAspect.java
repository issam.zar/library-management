package com.maidcc.library.logging;


import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class LoggingMethodPerformanceMetricsAspect {
    private static final Logger logger = LoggerFactory.getLogger(LoggingMethodPerformanceMetricsAspect.class);
    @Pointcut("execution(public * com.maidcc.library.model.service.impl..*.*(..))")
    public void publicMethods() {}

    @Around("publicMethods()")
    public Object logExecutionTime(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object result = proceedingJoinPoint.proceed();
        long elapsedTime = System.currentTimeMillis() - startTime;
        logger.info("Method [{}] executed in {} ms", proceedingJoinPoint.getSignature(), elapsedTime);
        return result;
    }
}
