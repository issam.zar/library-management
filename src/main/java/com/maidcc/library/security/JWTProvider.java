package com.maidcc.library.security;

import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class JWTProvider {
    @Value("${jwt.secret-key}")
    private String jwtSecretKey;
    @Value("${jwt.expiration-date}")
    private int expirationDate;

    public String generateToken(Authentication authentication){
        String username=authentication.getName();
        Date currentDAte=new Date();
        Date expDate=new Date(currentDAte.getTime()+expirationDate);

       return Jwts.builder()
               .setSubject(username)
               .setExpiration(expDate)
               .setIssuedAt(new Date())
               .signWith(SignatureAlgorithm.HS512,jwtSecretKey)
               .compact();
    }

    public String getUsername(String token){
        Claims claims= Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token).getBody();
        return  claims.getSubject();
    }

    public boolean validateToken(String token){
        try{
            Jwts.parser().setSigningKey(jwtSecretKey).parseClaimsJws(token);
            return true;
        }
        catch (SignatureException ex){
          throw new RuntimeException(ex.getMessage());
        }
        catch (MalformedJwtException ex){
            throw new RuntimeException(ex.getMessage());
        }
        catch (ExpiredJwtException ex){
            throw new RuntimeException(ex.getMessage());
        }
        catch (UnsupportedJwtException ex){
            throw new RuntimeException(ex.getMessage());
        }
        catch (IllegalArgumentException ex){
            throw new RuntimeException(ex.getMessage());
        }
    }

}
