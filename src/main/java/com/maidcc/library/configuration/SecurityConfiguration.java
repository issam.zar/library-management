package com.maidcc.library.configuration;


import com.maidcc.library.model.service.UserService;
import com.maidcc.library.security.AccessDeniedEntryPoint;
import com.maidcc.library.security.CustomAuthenticationEntryPoint;
import com.maidcc.library.security.JWTAuthFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class SecurityConfiguration {


    private UserService userService;
    private CustomAuthenticationEntryPoint authenticationEntryPoint;
    private AccessDeniedEntryPoint accessDeniedEntryPoint;

    private JWTAuthFilter jwtAuthFilter;

    private PasswordEncoder passwordEncoder;

    public SecurityConfiguration(UserService userService,
                                 CustomAuthenticationEntryPoint authenticationEntryPoint,
                                 AccessDeniedEntryPoint accessDeniedEntryPoint,
                                 JWTAuthFilter jwtAuthFilter,
                                 PasswordEncoder passwordEncoder
                                 ) {
        this.userService = userService;
        this.authenticationEntryPoint = authenticationEntryPoint;
        this.accessDeniedEntryPoint = accessDeniedEntryPoint;
        this.jwtAuthFilter=jwtAuthFilter;
        this.passwordEncoder= passwordEncoder;
    }



    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration authConfig) throws Exception {
        return authConfig.getAuthenticationManager();
    }





    @Bean
    public AuthenticationProvider authenticationProvider(){
        DaoAuthenticationProvider authenticationProvider=new DaoAuthenticationProvider();
        authenticationProvider.setPasswordEncoder(passwordEncoder);
        authenticationProvider.setUserDetailsService(userService);
        return authenticationProvider;
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf(httpSecurityCsrfConfigurer ->httpSecurityCsrfConfigurer.disable())
                .exceptionHandling(ex->ex.authenticationEntryPoint(authenticationEntryPoint))
                .exceptionHandling(ex->ex.accessDeniedHandler(accessDeniedEntryPoint))
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));


        httpSecurity.authorizeHttpRequests(req -> {
            req.requestMatchers(HttpMethod.POST, "/api/auth/**").permitAll();
            req.anyRequest().authenticated();
        });
        httpSecurity.authenticationProvider(authenticationProvider());
        httpSecurity.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class);

        return httpSecurity.build();
    }


}
